// TODO
// duplicate card check
// be more specific on TIE check - who's got the higher straight, better flush, better straight, suit power etc.
// loop check on straight. Ace can be a lower value
// optimise. the earliest checks are for the best hands

const VALUES = [
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "T",
  "J",
  "Q",
  "K",
  "A",
];
const SUITS = ["S", "H", "D", "C"];

const mapValueToNumber = {
  t: 10,
  j: 11,
  q: 12,
  k: 13,
  a: 14,
};

export const HAND = {
  ROYAL_FLUSH: 1,
  STRAIGHT_FLUSH: 2,
  FOUR_OF_A_KIND: 3,
  FULL_HOUSE: 4,
  FLUSH: 5,
  STRAIGHT: 6,
  THREE_OF_A_KIND: 7,
  TWO_PAIRS: 8,
  PAIR: 9,
  HIGH_CARD: 10,
};

const getCardsBySuit = (hand, suit) => {
  return hand
    .split(" ")
    .filter((card) => {
      return card.includes(suit);
    })
    .map((card) => {
      const value = card[0];
      return mapValueToNumber[value] || Number(value);
    });
};

export class PokerHand {
  constructor(hand) {
    this.validateHand(hand);
    const lowerCaseHand = hand.toLowerCase();

    const spades = getCardsBySuit(lowerCaseHand, "s");
    const diamonds = getCardsBySuit(lowerCaseHand, "d");
    const hearts = getCardsBySuit(lowerCaseHand, "h");
    const clubs = getCardsBySuit(lowerCaseHand, "c");
    this.hand = {
      S: spades,
      D: diamonds,
      H: hearts,
      C: clubs,
      all: [...spades, ...diamonds, ...hearts, ...clubs].sort((a, b) => a - b),
    };
  }

  validateHand(hand) {
    //validate
    if (typeof hand !== "string") {
      throw Error("string must be provided for pokerHand");
    }

    const cards = hand.split(" ");

    const invalidCards = cards.filter((card) => card.length !== 2);

    if (invalidCards.length) {
      throw Error(`invalid cards provided: "${invalidCards}"`);
    }

    if (cards.length !== 5) {
      throw Error("5 cards must be provided");
    }

    const incorrectValuesOrSuits = cards.reduce((acc, curr) => {
      const [value, suit] = curr.split("");

      //value and suit check
      if (!VALUES.includes(value) || !SUITS.includes(suit)) {
        return [...acc, curr];
      }

      return acc;
    }, []);

    if (incorrectValuesOrSuits.length) {
      throw Error(`value or suit of cards invalid ${incorrectValuesOrSuits}`);
    }
  }

  isSequential() {
    return this.hand["all"].every((value, i) => {
      if (i === this.hand["all"].length - 1) {
        return true;
      }

      return value + 1 === this.hand["all"][i + 1];
    });
  }

  getBestHand() {
    //same suit?
    if (
      SUITS.find((suit) => {
        return this.hand[suit].length === 5;
      })
    ) {
      //is it sequential?
      if (this.isSequential()) {
        if (
          Math.max(...this.hand["all"]) === 14 &&
          Math.min(...this.hand["all"]) === 10
        ) {
          return HAND.ROYAL_FLUSH;
        }

        return HAND.STRAIGHT_FLUSH;
      }

      return HAND.FLUSH;
    }

    if (this.isSequential()) {
      return HAND.STRAIGHT;
    }

    const values = this.hand["all"];

    const valueToOccurences = values.reduce((acc, curr) => {
      if (acc[curr]) {
        acc[curr] = acc[curr] + 1;
      } else {
        acc[curr] = 1;
      }
      return acc;
    }, {});

    const isFourOfAKind = Object.keys(valueToOccurences).find((value) => {
      return valueToOccurences[value] === 4;
    });

    if (isFourOfAKind) {
      return HAND.FOUR_OF_A_KIND;
    }

    const isThreeOfAKind = Object.keys(valueToOccurences).find((value) => {
      return valueToOccurences[value] === 3;
    });

    const isPair = Object.keys(valueToOccurences).filter((value) => {
      return valueToOccurences[value] === 2;
    });

    if (isThreeOfAKind) {
      if (isPair.length) {
        return HAND.FULL_HOUSE;
      }

      return HAND.THREE_OF_A_KIND;
    }

    if (isPair.length > 1) {
      return HAND.TWO_PAIRS;
    }

    if (isPair.length) {
      return HAND.PAIR;
    }

    return HAND.HIGH_CARD;
  }

  compareWith(pokerHand) {
    const myHand = this.getBestHand();
    const otherHand = pokerHand.getBestHand();
    if (myHand < otherHand) {
      return Result.WIN;
    }

    if (myHand > otherHand) {
      return Result.LOSS;
    }

    return Result.TIE;
  }
}

export const Result = {
  WIN: 1,
  LOSS: 2,
  TIE: 3,
};

export default PokerHand;
