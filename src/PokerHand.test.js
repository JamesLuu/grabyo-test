import PokerHand, { Result, HAND } from "./PokerHand.js";

const handsTestData = {
  royalFlush: "QH TH KH JH AH",
  straightFlush: "QD TD KD JD 9D",
  flush: "QC 5C KC JC 9C",
  straight: "7H 5D 6C 8S 9C",
  fourOfAKind: "QH QS QD JH QC",
  fullHouse: "9H 8S 8D 9D 8C",
  threeOfAKind: "6H 7S 7D TH 7C",
  twoPairs: "2H AS 2D TH AC",
  pair: "4H QS 2D 4D AC",
  highCard: "3H QS 2D 4D AC",
};

describe("PokerHand", () => {
  describe("validateHand()", () => {
    it("throws error if a string is NOT provided", () => {
      expect(() => new PokerHand(123)).toThrow();
    });

    it("throws an error if there are cards which do not have a number and a suit", () => {
      expect(() => new PokerHand("AC S 5S 8 2")).toThrow();
    });

    it("throws an error if there are more than 5 cards given", () => {
      expect(() => new PokerHand("AC 4S 5S 8C AH 6S")).toThrow();
    });

    it("throws an error if there are cards where the value is NOT 2-9, T, J, Q, K or A", () => {
      expect(() => new PokerHand("AC 4S 5S BC AH")).toThrow();
    });

    it("throw an error if it`s NOT a valid suit", () => {
      expect(() => new PokerHand("AC 4A 5S BC AH")).toThrow();
    });
  });

  describe("getBestHand()", () => {
    it("returns a ROYAL FLUSH when all cards are same suit and the top value is an ace and lowest value is 10", () => {
      const hand = new PokerHand(handsTestData.royalFlush);

      expect(hand.getBestHand()).toBe(HAND.ROYAL_FLUSH);
    });

    it("returns STRAIGHT_FLUSH when all cards are same suit and in sequence", () => {
      const hand = new PokerHand(handsTestData.straightFlush);

      expect(hand.getBestHand()).toBe(HAND.STRAIGHT_FLUSH);
    });

    it("returns a FLUSH if all cards are the same suit only", () => {
      const hand = new PokerHand(handsTestData.flush);

      expect(hand.getBestHand()).toBe(HAND.FLUSH);
    });

    it("returns a STRAIGHT when the cards are sequential but not the same suit", () => {
      const hand = new PokerHand(handsTestData.straight);

      expect(hand.getBestHand()).toBe(HAND.STRAIGHT);
    });

    it("returns FOUR_OF_A_KIND if there are 4 cards of the same value", () => {
      const hand = new PokerHand(handsTestData.fourOfAKind);

      expect(hand.getBestHand()).toBe(HAND.FOUR_OF_A_KIND);
    });

    it("returns FULL_HOUSE if there are three cards of the same value and another two cards of the same value", () => {
      const hand = new PokerHand(handsTestData.fullHouse);

      expect(hand.getBestHand()).toBe(HAND.FULL_HOUSE);
    });

    it("returns THREE_OF_A_KIND if there are three cards of the same value", () => {
      const hand = new PokerHand(handsTestData.threeOfAKind);

      expect(hand.getBestHand()).toBe(HAND.THREE_OF_A_KIND);
    });

    it("returns TWO_PAIRS if there are two cards of the same value and another two cards of same value", () => {
      const hand = new PokerHand(handsTestData.twoPairs);

      expect(hand.getBestHand()).toBe(HAND.TWO_PAIRS);
    });

    it("returns PAIR if there are two cards of the same value only", () => {
      const hand = new PokerHand(handsTestData.pair);

      expect(hand.getBestHand()).toBe(HAND.PAIR);
    });

    it("returns HIGH_CARD if no other rules are met", () => {
      const hand = new PokerHand(handsTestData.highCard);

      expect(hand.getBestHand()).toBe(HAND.HIGH_CARD);
    });
  });

  describe("compareWith()", () => {
    test.each([
      [handsTestData.royalFlush, handsTestData.royalFlush],
      [handsTestData.straightFlush, handsTestData.straightFlush],
      [handsTestData.fourOfAKind, handsTestData.fourOfAKind],
      [handsTestData.fullHouse, handsTestData.fullHouse],
      [handsTestData.flush, handsTestData.flush],
      [handsTestData.straight, handsTestData.straight],
      [handsTestData.threeOfAKind, handsTestData.threeOfAKind],
      [handsTestData.twoPairs, handsTestData.twoPairs],
      [handsTestData.pair, handsTestData.pair],
      [handsTestData.highCard, handsTestData.highCard],
    ])("ties", (myHand, otherHand) => {
      const hand1 = new PokerHand(myHand);
      const hand2 = new PokerHand(otherHand);
      expect(hand1.compareWith(hand2)).toBe(Result.TIE);
    });

    test.each([
      [handsTestData.royalFlush, handsTestData.straightFlush],
      [handsTestData.straightFlush, handsTestData.fourOfAKind],
      [handsTestData.fourOfAKind, handsTestData.fullHouse],
      [handsTestData.fullHouse, handsTestData.flush],
      [handsTestData.flush, handsTestData.straight],
      [handsTestData.straight, handsTestData.threeOfAKind],
      [handsTestData.threeOfAKind, handsTestData.twoPairs],
      [handsTestData.twoPairs, handsTestData.pair],
      [handsTestData.pair, handsTestData.highCard],
    ])("wins", (myHand, otherHand) => {
      const hand1 = new PokerHand(myHand);
      const hand2 = new PokerHand(otherHand);
      expect(hand1.compareWith(hand2)).toBe(Result.WIN);
    });

    test.each([
      [handsTestData.straightFlush, handsTestData.royalFlush],
      [handsTestData.fourOfAKind, handsTestData.straightFlush],
      [handsTestData.fullHouse, handsTestData.fourOfAKind],
      [handsTestData.flush, handsTestData.fullHouse],
      [handsTestData.straight, handsTestData.flush],
      [handsTestData.threeOfAKind, handsTestData.straight],
      [handsTestData.twoPairs, handsTestData.threeOfAKind],
      [handsTestData.pair, handsTestData.twoPairs],
      [handsTestData.highCard, handsTestData.pair],
    ])("loses", (myHand, otherHand) => {
      const hand1 = new PokerHand(myHand);
      const hand2 = new PokerHand(otherHand);
      expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
    });
  });
});
